fetch('https://jsonplaceholder.typicode.com/todos')
  .then((response) => response.json())
  .then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos')
  .then((response) => response.json())
  .then((json) =>
    console.log(
      json.map((todos) => {
        const container = {};

        container['title'] = todos.title;
        return container;
      })
    )
  );

fetch('https://jsonplaceholder.typicode.com/todos/28')
  .then((response) => response.json())
  .then((json) => console.log(json.title));

fetch('https://jsonplaceholder.typicode.com/todos/28').then((response) =>
  console.log(response.status)
);

fetch('https://jsonplaceholder.typicode.com/todos', {
  method: 'POST',
  headers: {
    'Content-type': 'application/json',
  },
  body: JSON.stringify({
    userId: 1,
    title: 'New Todo',
    completed: false,
  }),
})
  .then((response) => response.json())
  .then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/28', {
  method: 'PUT',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify({
    id: 10,
    userId: 101,
    title: 'Updated Todo',
    completed: true,
  }),
})
  .then((response) => response.json())
  .then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/28', {
  method: 'PUT',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify({
    title: 'Updated Todo',
    description: 'Describing todolist',
    status: 'Pending',
    dateCompleted: '2022-11-29',
    userID: 101,
  }),
})
  .then((response) => response.json())
  .then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/28', {
  method: 'PATCH',
  headers: {
    'Content-type': 'application/json',
  },
  body: JSON.stringify({
    title: 'This property only changed',
  }),
})
  .then((response) => response.json())
  .then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/28', {
  method: 'PATCH',
  headers: {
    'Content-type': 'application/json',
  },
  body: JSON.stringify({
    completed: true,
    dateCompleted: '2022-11-29',
  }),
})
  .then((response) => response.json())
  .then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/28', {
  method: 'DELETE',
});
